package sqltesting

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"testing"
)

func DBMock(t *testing.T) (*sql.DB, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	require.Nil(t, err)
	return db, mock
}

func SafelyCloseMock(t *testing.T, mock sqlmock.Sqlmock) {
	require.Nil(t, mock.ExpectationsWereMet())
}

func SafelyCloseDB(t *testing.T, db *sql.DB) {
	err := db.Close()
	if err != nil {
		require.Contains(t, err.Error(), "all expectations were already fulfilled")
	}
}
