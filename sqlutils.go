package sqlutils

import (
	"database/sql"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
)

func CloseRowsErr(rows *sql.Rows, err error) error {
	rowsErr := rows.Close()
	if rowsErr != nil {
		return multierror.Append(errors.Wrap(rowsErr, "failed to close rows"), err)
	}
	return err
}

func CloseRowsErrWrap(rows *sql.Rows, err error, message string) error {
	return CloseRowsErr(rows, errors.Wrap(err, message))
}

func CloseRowsErrWrapf(rows *sql.Rows, err error, format string, args ...interface{}) error {
	return CloseRowsErr(rows, errors.Wrapf(err, format, args...))
}

func RollbackTxErr(tx *sql.Tx, err error) error {
	txErr := tx.Rollback()
	if txErr != nil {
		return multierror.Append(errors.Wrap(txErr, "failed to rollback transaction"), err)
	}
	return err
}

func RollbackTxErrWrap(tx *sql.Tx, err error, message string) error {
	return RollbackTxErr(tx, errors.Wrap(err, message))
}

func RollbackTxErrWrapf(tx *sql.Tx, err error, format string, args ...interface{}) error {
	return RollbackTxErr(tx, errors.Wrapf(err, format, args...))
}
