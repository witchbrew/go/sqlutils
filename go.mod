module gitlab.com/witchbrew/go/sqlutils

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/hashicorp/go-multierror v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
)
