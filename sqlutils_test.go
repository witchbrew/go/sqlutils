package sqlutils

import (
	"errors"
	"github.com/hashicorp/go-multierror"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/sqlutils/sqltesting"
	"testing"
)

func TestCloseRowsErrWrapNoCloseError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	sqlmock.ExpectQuery("SELECT 1").WillReturnRows(sqlmock.NewRows([]string{"1"}))
	rows, err := db.Query("SELECT 1")
	require.Nil(t, err)
	customErr := errors.New("some error")
	closeErr := CloseRowsErrWrap(rows, customErr, "some close err")
	require.NotNil(t, closeErr)
	require.Equal(t, "some close err: some error", closeErr.Error())
}

func TestCloseRowsErrWrapCloseError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	mockRows := sqlmock.NewRows([]string{"1"})
	mockRows.CloseError(errors.New("inner close rows err"))
	sqlmock.ExpectQuery("SELECT 1").WillReturnRows(mockRows)
	rows, err := db.Query("SELECT 1")
	require.Nil(t, err)
	customErr := errors.New("some error")
	closeErr := CloseRowsErrWrap(rows, customErr, "some close err")
	require.NotNil(t, closeErr)
	mCloseErr := closeErr.(*multierror.Error)
	require.Equal(t, 2, len(mCloseErr.Errors))
	require.Equal(t, mCloseErr.Errors[0].Error(), "failed to close rows: inner close rows err")
	require.Equal(t, "some close err: some error", mCloseErr.Errors[1].Error())
}

func TestCloseRowsErrWrapfNoCloseError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	sqlmock.ExpectQuery("SELECT 1").WillReturnRows(sqlmock.NewRows([]string{"1"}))
	rows, err := db.Query("SELECT 1")
	require.Nil(t, err)
	customErr := errors.New("some error")
	closeErr := CloseRowsErrWrapf(rows, customErr, "some close err %d", 42)
	require.NotNil(t, closeErr)
	require.Equal(t, "some close err 42: some error", closeErr.Error())
}

func TestCloseRowsErrWrapfCloseError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	mockRows := sqlmock.NewRows([]string{"1"})
	mockRows.CloseError(errors.New("inner close rows err"))
	sqlmock.ExpectQuery("SELECT 1").WillReturnRows(mockRows)
	rows, err := db.Query("SELECT 1")
	require.Nil(t, err)
	customErr := errors.New("some error")
	closeErr := CloseRowsErrWrapf(rows, customErr, "some close err %d", 42)
	require.NotNil(t, closeErr)
	mCloseErr := closeErr.(*multierror.Error)
	require.Equal(t, 2, len(mCloseErr.Errors))
	require.Equal(t, mCloseErr.Errors[0].Error(), "failed to close rows: inner close rows err")
	require.Equal(t, "some close err 42: some error", mCloseErr.Errors[1].Error())
}

func TestRollbackTxErrWrapNoRollbackError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	sqlmock.ExpectBegin()
	tx, err := db.Begin()
	require.Nil(t, err)
	customErr := errors.New("some error")
	sqlmock.ExpectRollback()
	rbErr := RollbackTxErrWrap(tx, customErr, "some rollback err")
	require.NotNil(t, rbErr)
	require.Equal(t, "some rollback err: some error", rbErr.Error())
}

func TestRollbackTxErrWrapRollbackError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	sqlmock.ExpectBegin()
	tx, err := db.Begin()
	require.Nil(t, err)
	customErr := errors.New("some error")
	rbErr := RollbackTxErrWrap(tx, customErr, "some rollback err")
	require.NotNil(t, rbErr)
	mRbErr := rbErr.(*multierror.Error)
	require.Equal(t, 2, len(mRbErr.Errors))
	require.Contains(t, mRbErr.Errors[0].Error(), "failed to rollback transaction")
	require.Equal(t, "some rollback err: some error", mRbErr.Errors[1].Error())
}

func TestRollbackTxErrWrapfNoRollbackError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	sqlmock.ExpectBegin()
	tx, err := db.Begin()
	require.Nil(t, err)
	customErr := errors.New("some error")
	sqlmock.ExpectRollback()
	rbErr := RollbackTxErrWrapf(tx, customErr, "some rollback err %d", 42)
	require.NotNil(t, rbErr)
	require.Equal(t, "some rollback err 42: some error", rbErr.Error())
}

func TestRollbackTxErrWrapfRollbackError(t *testing.T) {
	db, sqlmock := sqltesting.DBMock(t)
	defer sqltesting.SafelyCloseMock(t, sqlmock)
	defer sqltesting.SafelyCloseDB(t, db)
	sqlmock.ExpectBegin()
	tx, err := db.Begin()
	require.Nil(t, err)
	customErr := errors.New("some error")
	rbErr := RollbackTxErrWrapf(tx, customErr, "some rollback err %d", 42)
	require.NotNil(t, rbErr)
	mRbErr := rbErr.(*multierror.Error)
	require.Equal(t, 2, len(mRbErr.Errors))
	require.Contains(t, mRbErr.Errors[0].Error(), "failed to rollback transaction")
	require.Equal(t, "some rollback err 42: some error", mRbErr.Errors[1].Error())
}
